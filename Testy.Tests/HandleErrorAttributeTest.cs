﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using HandleErrorAttribute = Testyweb.Infrastructure.HandleErrorAttribute;

namespace Testy.Tests
{
    public class HandleErrorAttributeTest
    {
        [TestFixture]
        public class OnException
        {
            private HandleErrorAttribute _errorAttribute;

            [SetUp]
            public void SetUp()
            {
                _errorAttribute = new HandleErrorAttribute();
            }

            [Test]
            public void IgnoresOnExceptionHandled()
            {
                ExceptionContext context = new ExceptionContext { ExceptionHandled = true };
                _errorAttribute.OnException(context);
            }

            [Test]
            public void SetsHandledOnNullException()
            {
                ExceptionContext context = new ExceptionContext { ExceptionHandled = false, Exception = null};
                _errorAttribute.OnException(context);

                Assert.IsTrue(context.ExceptionHandled);
            }

            [Test]
            public void TracesOnUnhandledException()
            {
                TraceListenerStub listenerStub = new TraceListenerStub();
                Trace.Listeners.Add(listenerStub);

                const string exceptionMessage = "some message";
                ExceptionContext context = new ExceptionContext { ExceptionHandled = false, Exception = new Exception(exceptionMessage)};
                _errorAttribute.OnException(context);

                listenerStub.IsReceived(exceptionMessage);
            }
        }
    }

    public class TraceListenerStub : TraceListener
    {
        private List<string> _calls;

        public TraceListenerStub()
        {
            _calls = new List<string>();
        }

        public override void Write(string message)
        {
            _calls.Add(message);
        }

        public override void WriteLine(string message)
        {
            _calls.Add(message);
        }

        public bool IsReceived(string message)
        {
            return _calls.Contains(message);
        }
    }
}
