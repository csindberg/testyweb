﻿using System;
using System.Web.Mvc;
using NUnit.Framework;
using Testyweb.Controllers;

namespace Testy.Tests
{
    public class HomeControllerTest
    {
        [TestFixture]
        public class Index
        {
            private HomeController _controller;

            [SetUp]
            public void SetUp()
            {
                _controller = new HomeController();
            }

            [Test]
            public void IndexShouldWork()
            {
                ActionResult result = _controller.Index();
                Assert.IsInstanceOf<ViewResult>(result);
            }
        }
        [TestFixture]
        public class SomeOtherAction
        {
            private HomeController _controller;

            [SetUp]
            public void SetUp()
            {
                _controller = new HomeController();
            }

            [Test]
            public void ShouldRedirectToPositiveOnPositiveInteger()
            {
                ActionResult result = _controller.SomeOtherAction(2);

                Assert.IsInstanceOf<RedirectResult>(result);
                Assert.AreEqual("/positive", ((RedirectResult)result).Url);
            }

            [Test]
            public void ShouldRedirectToNegativeOnZeroInteger()
            {
                ActionResult result = _controller.SomeOtherAction(0);

                Assert.IsInstanceOf<RedirectResult>(result);
                Assert.AreEqual("/negative", ((RedirectResult)result).Url);
            }

            [Test]
            public void ShouldRedirectToNegativeOnNegativeInteger()
            {
                ActionResult result = _controller.SomeOtherAction(-3);

                Assert.IsInstanceOf<RedirectResult>(result);
                Assert.AreEqual("/negative", ((RedirectResult)result).Url);
            }
        }

        [TestFixture]
        public class UntestedAction
        {
            private HomeController _controller;

            [SetUp]
            public void SetUp()
            {
                _controller = new HomeController();
            }

            [Test]
            public void ShouldFailOnNullInput()
            {
                Assert.Throws<InvalidOperationException>(() => { _controller.UntestedAction(null); });
            }

            [Test]
            public void ShouldFailOnEmptyInput()
            {
                Assert.Throws<InvalidOperationException>(() => { _controller.UntestedAction(string.Empty); });
            }

            [Test]
            [TestCase("/yolo")]
            [TestCase("/dingo")]
            [TestCase("/dassy")]
            [TestCase("roflo")]
            [TestCase("sdsdsd")]
            public void ShouldRedirectOnValidInput(string input)
            {
                ActionResult result = _controller.UntestedAction(input);

                Assert.IsInstanceOf<RedirectResult>(result);
                Assert.AreEqual(input, ((RedirectResult)result).Url);
            }
        }
    }
}