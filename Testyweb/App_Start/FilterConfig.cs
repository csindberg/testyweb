﻿using System.Diagnostics.CodeAnalysis;
using Testyweb.Infrastructure;

namespace Testyweb.App_Start
{
    public class FilterConfig
    {
        [ExcludeFromCodeCoverage]
        public static void Configure(System.Web.Mvc.GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
