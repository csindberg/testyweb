﻿using System;
using System.Configuration;
using System.Web.Mvc;

namespace Testyweb.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            string environment = ConfigurationManager.AppSettings["Environment"];
            return View((object)environment);
        }

        public ActionResult SomeOtherAction(int value)
        {
            if (value > 0)
                return Redirect("/positive");

            return Redirect("/negative");
        }

        public ActionResult UntestedAction(string input)
        {
            if(string.IsNullOrEmpty(input))
                throw new InvalidOperationException();

            return Redirect(input);
        }
    }
}